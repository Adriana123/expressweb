@extends('layouts.master')
 
@section('content')
<div class="menu-box"></div>
<div class="about-section-box">
	<div class="container card " style="max-width: 25rem;">
		<div class="row">
			<div class="col-lg-12">
				<div class="card-header bg-warning heading-title text-center">
					<h2>Regístrate</h2>
				</div>
			</div>
		</div>
		<form>
            <div class="mb-3 ">
                <label for="exampleInputEmail1" class="form-label">Ingresa tu correo</label>
                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required>
            </div>
            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">Contraseña</label required>
                <input type="password" class="form-control" id="exampleInputPassword1">
            </div class="mb-3">
                <input required type="password"id="contraseniaAdmin" name="contraseniaAdmin" class="form-control">
                <label for="usuario">Confirmar contraseña</label>
                <input required type="password"id="contraseniaAdmin" name="contraseniaAdmin" class="form-control">
            <div>
            </div>
            <div class="card-footer" align="center"> 
             <button type="submit" class="btn btn-lg btn-circle btn-outline-new-white">Registrar</button>
                <a href="{{ url('/login') }}" class="btn btn-light">
                    <img src="{{ url('assets/bootstrap/bootstrap-icons/chevron-compact-left.svg')}} ">
                    Volver
                </a>
            </div>
        </form>
	</div>
</div>
</div>
@stop