<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Site Metas -->
    <title>ExpressWeb</title>  
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="https://thumbs.dreamstime.com/z/icono-del-perno-mapa-restaurante-elemento-amonestador-de-la-navegaci%C3%B3n-para-los-apps-m%C3%B3viles-concepto-y-web-detallado-118830803.jpg" type="image/x-icon">


    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ url('/assets/plantilla/css/bootstrap.min.css') }}">
    <!-- Site CSS -->
    <link rel="stylesheet" href="{{ url('/assets/plantilla/css/style.css') }}">    
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="{{ url('/assets/plantilla/css/responsive.css') }}">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ url('/assets/plantilla/css/custom.css') }}">

    <title>ExpressWeb</title>
  </head>
  <body>
    <header>
      @include('partials.navbar')
    </header>
    

    <!-- Sección principal donde estará el contenido de la página web -->
    <divclass="container">
        @yield('content')
    </div>

    <footer>
      @include('partials.footer')
    </footer>

    <a href="#" id="back-to-top" title="Back to top" style="display: none;"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></a>

    <!-- ALL JS FILES -->
    
    <script src="{{ url('/assets/plantilla/js/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ url('/assets/plantilla/js/popper.min.js') }}"></script>
    <script src="{{ url('/assets/plantilla/js/bootstrap.min.js') }}"></script>
      <!-- ALL PLUGINS -->
    <script src="{{ url('/assets/plantilla/js/jquery.superslides.min.js') }}"></script>
    <script src="{{ url('/assets/plantilla/js/images-loded.min.js') }}"></script>
    <script src="{{ url('/assets/plantilla/js/isotope.min.js') }}"></script>
    <script src="{{ url('/assets/plantilla/js/baguetteBox.min.js') }}"></script>
    <script src="{{ url('/assets/plantilla/js/form-validator.min.js') }}"></script>
    <script src="{{ url('/assets/plantilla/js/contact-form-script.js') }}"></script>
    <script src="{{ url('/assets/plantilla/js/custom.js') }}"></script>></script>
  </body>
</html>