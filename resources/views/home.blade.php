@extends('layouts.master')
 
@section('content')
    <!-- Start slides -->
	<div id="slides" class="cover-slides">
		<ul class="slides-container">
			<li class="text-left">
				<img src="https://cdn.shortpixel.ai/spai/w_750+q_lossy+ret_img+to_webp/https://snob.mx/wp-content/uploads/2014/05/Sliders_23.jpg" alt="">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h1 class="m-b-20"><strong>Bienvenido a <br> ExpressWeb</strong></h1>
							<p class="m-b-40">Te ofrecemos una plataforma online simple, pŕactica y llamativa para realizar tus pedidos de comida a domicilio, sólo necesitas dar un click para ordenar lo que deseas.</p>
							<p><a class="btn btn-lg btn-circle btn-outline-new-white" href="{{url('/menu')}}">Has tú domicilio ahora</a></p>
						</div>
					</div>
				</div>
			</li>
			<li class="text-left">
				<img src="https://www.adrianasbestrecipes.com/wp-content/uploads/2019/09/Vegetarian-Menu-for-Game-Day.jpg" alt="">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h1 class="m-b-20"><strong>ExpressWeb <br> Fácil y rápido</strong></h1>
							<p><a class="btn btn-lg btn-circle btn-outline-new-white" href="{{url('/menu')}}">¡Queremos hacerte la vida más fácil!</a></p>
						</div>
					</div>
				</div>
			</li>
		</ul>
		<div class="slides-navigation">
			<a href="#" class="next"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
			<a href="#" class="prev"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
		</div>
	</div>
	<!-- End slides -->
	
	<!-- Start About -->
	<div class="about-section-box">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-12 text-left">
					<div align="center">
						<h1>Domicilios ExpressWeb</h1>
						<p>Pide tu comida favorita con las mejores ofertas.</p>
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12">
					<img src="./assets/plantilla/images/stuff-img-05.jpg" alt="aquí la imagen">
				</div>
			</div>
		</div>
	</div>
	<!-- End About -->
	
	<!-- Start QT -->
	<div class="qt-box qt-background">
		<div class="container">
			<div class="row">
				<div class="col-md-8 ml-auto mr-auto text-center">
					<span class="lead ">
						"Si somos lo que comemos, somos toda una delicia."
					</span>
				</div>
			</div>
		</div>
	</div>
	<!-- End QT -->

	
@stop