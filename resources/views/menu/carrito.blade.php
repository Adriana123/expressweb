@extends('layouts.master')
 
@section('content')
<div class="menu-box">
<div class="row" style="margin-top:40px">
    <div class="col-md-2" align="center"></div>
        <div class="col-md-7" align="center"><br>
            <table class="table"><br>
                <div class="table-warning"><h2>Tus Pedidos<h2></div>
                    <tr>
                        <th scope="col"><h5>Plato</h5></th>
                        <th scope="col"><h5>Valor Unitario</h5></th>
                        <th scope="col"><h5>Cantidad</h5></th>
                        <th scope="col"><h5>Valor Total</h5></th>
                        <th scope="col"><h5>Modo de Pago</h5></th>
                        <th scope="col"><h5>Foto</h5></th>
                        <th scope="col"><h5>Eliminar</h5></th>
                    </tr>
            <tbody>
            @php

                $cant = 0;
                $vf = 0;

            @endphp
            @foreach($carrito as $key => $carrito)
                <tr>
                    <td>{{$carrito->nom_plato}}</td>
                    @php
                        $fvalor= number_format($carrito->valor, 2, ",", ".");
                        $fvt = number_format($carrito->valor_total, 2, ",", ".");
                    @endphp
                    <td>$ @php echo $fvalor @endphp</td>
                    <td>{{$carrito->cantidad}}</td>
                    <td>$ @php echo $fvt @endphp</td>
                    <td>{{$carrito->modo_pago}}</td>
                    <td>
                        <img src="{{$carrito->foto}}" width="50" height="50"/>
                    </td>
                    <td> 
                    <form action =" " method="POST" style="display:inline">
					    {{csrf_field()}}
                        <input type="text" name="pedido" id="pedido" value="{{$carrito->id_pedido}}" readonly style="display:none;">
						<button type="submit" class="btn-sm btn-danger" style="display:inline;cursor;pointer; color:black">
							Eliminar
						</button>
				    </form>
                    </td>
                </tr>
                @php 
                    $cant += $carrito->cantidad;
                    $vf+= $carrito->valor_total;
                    if($vf != 0){
                        $formato_vf = number_format($vf,2, ",", ".");
                    }
                @endphp
            @endforeach
            </tbody>
            </table>
        </div>
        <div class=" menu-box col-md-2" style="width: 18rem;" align="center">
            <div class="card border-warning">
                <div class="card bg-warning heading-title text-center">
                    <h3>Resumen</h3>
                </div>
                <div class="card-text text-dark" >
                
                    <p> Total productos: @php echo $cant @endphp<br>
                        Total valor pedidos: <br>
                        $ @php echo $vf!=0 ? $formato_vf : $vf @endphp
                    </p>
                    <a href="{{ url('/menu') }}" class="btn-sm btn-success" >
                        Volver
                    </a><hr>
                </div>
            </div>
        </div>
        
    </div>
</div>
</div>
@stop