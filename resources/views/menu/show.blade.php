@extends('layouts.master')
@section('content')

<!-- Start All Pages -->
<div class="all-page-title page-breadcrumb">
		<div class="container text-center">
			<div class="row">
				<div class="col-lg-12">
					<h1>Menú</h1>
				</div>
			</div>
		</div>
	</div>
	
	<div class="menu-box">
	<div class="container">

    @foreach($plato as $key => $plato )   
    <div class="row">       

        <div class="col-sm-4" align="center">           
            {{-- TODO: Imagen del plato --}}  
            <img style="border: 1px solid;" src="{{$plato->foto}}" width="250" height="250" />    
        </div>      

        <div class="col-sm-8">           
            {{-- TODO: Datos del plato --}}
            <h1><strong>{{$plato->nom_plato}}</strong></h1>
            <h2>Tiempo de preparación: {{$plato->tiempo_preparacion}}</h2>
            @php 
                $num= number_format($plato->valor, 2, ",", ".");
            @endphp
                <h2><strong>Precio: $@php echo $num @endphp</strong></h3>
            <p>
                <strong>Descripción: </strong>{{$plato->descripcion}}
            </p>

            <p>
            <a type="button" href="{{ url('menu/pedido/' .$plato->id_platos) }}" class="btn btn-primary">Pedir</a>
                
                @can('admin.edit')		
                <a href="{{ url('/menu/edit/' . $plato->id_platos ) }}" class="btn btn-warning">
                    Editar</a>
                
                               	
                <form action =" " method="POST" style="display:inline">
					{{csrf_field()}}
                    @method('DELETE')
                    <input type="text" name="plato" id="plato" value="{{ $plato->id_platos }}" readonly style="display:none;">
						<button type="submint" class="btn btn-danger" style="display:inline;cursor;pointer; color:white">
							Borrar
						</button>
                                        
				</form>
                @endcan
                <a href="{{ url('/menu') }}" class="btn btn-outline-new-white" >Volver</a>

                
            </p>
            
        </div>
    </div> 
    @endforeach
    </div>
    </div>
</div>

@stop
