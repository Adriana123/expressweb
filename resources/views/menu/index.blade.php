@extends('layouts.master')
 
@section('content')
	<!-- Start All Pages -->
	<div class="all-page-title page-breadcrumb">
		<div class="container text-center">
			<div class="row">
				<div class="col-lg-12">
					<h1>Menú</h1>
				</div>
			</div>
		</div>
	</div>
	<!-- End All Pages -->
	<!-- Start Menu -->
	<a class="nav-link btn btn-space2 btn-outline-new-white" href="{{ url('/menu/carrito')}}"> 
		    Ver Carrito</a>	
	@can('admin.create')
		<a class="nav-link btn btn-space btn-outline-new-white" href="{{ url('/menu/create')}}"> 
		    Crear Plato</a>	
  	@endcan
	<div class="menu-box">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="heading-title text-center">
						<h2>¡Pide lo que te gusta y al mejor precio!</h2>
					</div>
				</div>
			</div>
			
			<div class="row">
				@foreach($arrayPlato as $key => $plato)
					<div class="col-xs-6 col-sm-4 col-md-3 text-center"> 
						<a href="{{ url('/menu/show/' . $plato->id_platos ) }}"> 
							<img src="{{$plato->foto}}"width="250" height="250"/>
							<h4 align="center"><strong>{{$plato->nom_plato}}</strong></h4>
						</a>
					</div>
				@endforeach
			</div>

		</div>
	</div>
	
	<!-- End Menu -->
@stop 
				
				
			
