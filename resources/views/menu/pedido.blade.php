@extends('layouts.master')
@section('content')

<div class="all-page-title page-breadcrumb">
	<div class="container text-center">
		<div class="row">
			<div class="col-lg-12">
				<h1>Pedido</h1>
			</div>
		</div>
	</div>
</div>

	<div class="menu-box">
	    <div class="container card  border-danger" >
            @foreach($plato as $key => $plato )   
            <div class="row">       
                <div class="col-sm-6" align="center" >
                    <br><br>          
                    <img style="border: 1px solid;" src="{{$plato->foto}}" width="300" height="300" /><br><br>
                         
                </div> 
                
                <form action="{{url('/menu/pedido/' . $plato->id_platos)}}" method='POST'>
                    {{-- TODO: Protección contra CSRF --}}
                    {{ csrf_field() }}
                    <div class="col-sm-0">           
                        <h1><strong>{{$plato->nom_plato}}</strong></h1>
                        <h4>Precio: $
                        <input class="input-group input-group-sm mb-3" readonly="readonly" type="number" id="valor" name="valor" value="{{$plato->valor}}"/></h4>
                        
                        <h4>Cantidad del pedido: </h4>
                        <input class="input-group input-group-sm mb-3" required type="number" min="1" id="cantidad" name="cantidad"/>

                        <h4>Ingrese su dirección:
                            <input class="input-group input-group-sm mb-3" required type="text" id="direccion" name="direccion" />
                        </h4>
                        <div>
                            <h3>Seleccione el modo de pago: </h3>
                            <input required type="radio" id="modo_pago" name="modo_pago" value="efectivo"/>Efectivo&nbsp;
                            <input required type="radio" id="modo_pago" name="modo_pago" value="tarjeta"/>Tarjeta
                        </div>
                        
                        <div align="left">
                        <button type="submit" class="btn btn-lg btn-space btn-circle btn-outline-new-white" >
                            Agregar al carrito
                        </button>
                        </div>                      
                    </div>
                </form>
                
                <script>document.getElementById('valor').value=parseInt('{{$plato->valor}}');</script>
            </div> 
            @endforeach       
        </div>
    </div>  
</div>
    <div class="row justify-content-center" >
        <a href="{{ url('/menu/show/' . $plato->id_platos)}}" class="btn btn-space btn-outline-new-white">Volver</a> 
    </div>
@stop