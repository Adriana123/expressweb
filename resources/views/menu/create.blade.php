@extends('layouts.master')
 
@section('content')
@role('admin')
<div class="about-section-box">
    <div class="row" style="margin-top:40px">
    <div class="offset-md-3 col-md-6">
        <div class="card menu-box ">
            <div class="card-header bg-warning heading-title text-center">
                <h2>Agregar Plato</h2>
            </div>
            <div class="card-body" style="padding:30px">

                {{-- TODO: Abrir el formulario e indicar el método POST --}}
                <form action="{{ url('menu/create') }}" method='POST'enctype="multipart/form-data">
                    {{-- TODO: Protección contra CSRF --}}
                    {{ csrf_field() }}

                        <div class="form-group">
                            <label>Nombre del plato</label>
                            <input type="text" name="nom_plato" id="nom_plato" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label>Tiempo de preparación</label>
                            <input type="text" name="tiempo_preparacion" id="tiempo_preparacion" class="form-control" required />
                        </div>

                        <div class="form-group">
                            <label>Precio</label>
                            <input type="number" min="5000" name="valor" id="valor" class="form-control" required />
                        </div>

                        <div class="form-group">
                            <labe>Descripción</label>
                            <input type="text" name="descripcion" class="form-control" required />
                        </div>

                        <div class="form-group">
                            <label>Subir imagen del plato</label>
                            <input type="file" name="foto" id="foto" required />
                        </div>

                        <div class="form-group text-center">
                        <button type="submit" class="btn btn-lg btn-circle btn-outline-new-white">
                            Agregar
                        </button>
                        </div>
                </form>
            </div>
        </div>
    </div>
    </div>
</div>
@else
<div class="all-page-title page-breadcrumb">
	<div class="container text-center">
		<div class="row">
			<div class="col-lg-12">
				<h1>No eres administrador</h1>
			</div>
		</div>
	</div>
</div>
<div class="menu-box">
    <a href="{{ url('/menu')}}" class="btn btn-outline-new-white btn-space">Volver</a>
</div>    
@endrole
@stop