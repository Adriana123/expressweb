	<!-- Start header -->
	<header class="top-navbar">
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<div class="container">
				<a class="navbar-brand" href="{{ url('/')}}">
					<img src="/assets/plantilla/images/icono.jpeg" alt="" width="150" height="100" />
				</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbars-rs-food" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
				  <span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbars-rs-food">
					<ul class="navbar-nav ml-auto">
						<li class="nav-item active"><a class="nav-link" href="{{ url('/')}}">Home</a></li>
						<li class="nav-item dropdown"> 
							<li class="nav-item" class="nav-link"><a class="nav-link" href="{{ url('/menu/')}}"> 
							Menú</a></li>
							
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="dropdown-a" data-toggle="dropdown">Sesión</a>
							<div class="dropdown-menu" aria-labelledby="dropdown-a">
								<a class="dropdown-item" href="{{ url('/register')}}">Regístrate</a>
								<a class="dropdown-item" href="{{ url('/login')}}">Iniciar Sesión</a>
							</div>
							<li class="nav-item dropdown">
							@if(Auth::check())
							<div>
								<form action ="{{url('logout')}}" method="POST" style="display:inline">
								{{csrf_field()}}
									<button type="submit" class="btn btn-link nav-link">
										Cerrar Sesión
									</button>
								</form>
							</div>
							@endif
						</
						</li>
					</ul>
				</div>
			</div>
		</nav>
	</header>
	<!-- End header -->