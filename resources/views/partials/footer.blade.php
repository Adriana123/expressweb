<footer class="footer-area bg-f">
		<div class="container">
			<div class="row">
				<div class="col">
					<h3>Acerca de nosotros</h3>
					<p>Estudiantes Universidad de Nariño <br>Décimo Semestre
					<a href="https://www.instagram.com/kandel_yandar/"class="nav-link">Kandel Yandar</a>
					<a href="https://www.instagram.com/adri_calpa/" class="nav-link">Adriana Calpa</a>
					</p>
				</div>
				<div class="col">
					<h3>Información de Contacto</h3>
					<p class="lead">Colombia, Departamento de Nariño, Pasto, Calle 18A #34-47</p>
					<p class="lead"><a href="#">+01 2000 800 9999</a></p>
					<p><a href="#"> expressweb@.gmail.com</a></p>
				</div>
			</div>
		</div>
		
		<div class="copyright">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<p class="company-name">&copy; 2021 @<a href="/">ExpressWeb</a>
					</div>
				</div>
			</div>
		</div>

		<!-- Start Contact info -->
	<div class="contact-imfo-box">
		<div class="container">
			<div class="row">
				<div class="col-md-4 arrow-right">
					<i class="fa fa-volume-control-phone"></i>
					<div class="overflow-hidden">
						<h4>Teléfono</h4>
						<p class="lead">
							+01 123-456-4590
						</p>
					</div>
				</div>
				<div class="col-md-4 arrow-right">
					<i class="fa fa-envelope"></i>
					<div class="overflow-hidden">
						<h4>Email</h4>
						<p class="lead">
							expressweb@gmail.com
						</p>
					</div>
				</div>
				<div class="col-md-4">
					<i class="fa fa-map-marker"></i>
					<div class="overflow-hidden">
						<h4>Ubicación</h4>
						<p class="lead">
							Pasto, Calle 18A #34-47
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Contact info -->
</footer>
