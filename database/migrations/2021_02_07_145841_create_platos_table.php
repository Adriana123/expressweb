<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlatosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('platos', function (Blueprint $table) {
            $table->engine = 'InnoDB'; //InnoB: motor de almacenamiento que soporta trabajar con llaves foráneas y la integridad referencial
            $table->bigIncrements('id_platos');
            $table->string('nom_plato');
            $table->text('descripcion');
            $table->integer('valor');
            $table->string('tiempo_preparacion');
            $table->string('foto');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('platos');
        Schema::dropIfExists('platos');
    }
}
