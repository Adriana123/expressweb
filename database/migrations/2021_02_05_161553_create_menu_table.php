<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('menus', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id_menu');
            $table->bigInteger('id_plato')->unsigned();
            $table->timestamps();
       });
    }
 
   /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    { 
       Schema::drop('menus');
       Schema::dropIfExists('menus');
     
    }

}
