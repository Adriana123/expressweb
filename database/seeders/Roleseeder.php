<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role1 = Role::create(['name' => 'admin']);
        $role2 = Role::create(['name' => 'usuario']);


        $permission = Permission::create(['name' => 'admin.edit'])->assignRole($role1);
        $permission = Permission::create(['name' => 'admin.create'])->assignRole($role1);
        $permission = Permission::create(['name' => 'home'])->syncRoles([$role1, $role2]);
        $permission = Permission::create(['name' => 'au.menu'])->syncRoles([$role1, $role2]);
        $permission = Permission::create(['name' => 'au.show'])->syncRoles([$role1, $role2]);
        $permission = Permission::create(['name' => 'au.pedido'])->syncRoles([$role1, $role2]);
        $permission = Permission::create(['name' => 'dashboard'])->syncRoles([$role1, $role2]);
        $permission = Permission::create(['name' => 'au.carrito'])->assignRole([$role2]);

    }
}
