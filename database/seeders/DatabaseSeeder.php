<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Plato;
use App\Models\Menu;
use App\Models\Pedido;

class DatabaseSeeder extends Seeder
{
    
    private $arrayPlato = array(
		array(
			'nom_plato' => 'Hamburguesa',
			'descripcion' => 'Carne de res, costilla de cerdo en salsa bbq, lechuga, tocineta crujiente, queso philadelphia, papas rústicas.', 
			'valor' => 15000, 
			'tiempo_preparacion' => '30 min', 
			'foto' => 'https://images.rappi.com/products/71d234b1-66eb-4e08-ab9a-02eccb69a946-1601574699022.jpeg?d=400x400&e=webp', 
		),
        array(
			'nom_plato' => 'Pizza Personal',
			'descripcion' => 'Pizza suave masa cheers, salsa napolitana, queso mozzarella. Puedes escoger entre nuestros 26 sabores.', 
			'valor' => 15000, 
			'tiempo_preparacion' => '25 min', 
			'foto' => 'https://images.rappi.com/products/e94cae03-7982-43fb-a6d2-b48f5083b7b9-1586986270641.png?d=400x400&e=webp', 
		),
        array(
			'nom_plato' => 'Crepes',
			'descripcion' => 'Crepe de banano, fresas, crema de avellanas con cacao, queso crema, helado sabor a elección y crema chantilly.', 
			'valor' => 13000, 
			'tiempo_preparacion' => '15 min', 
			'foto' => 'https://images.rappi.com/products/30dde4e0-6c73-4f04-a1db-d0ae5f3ff564-1600462212380.jpeg?d=400x400&e=webp', 
		),
        array(
			'nom_plato' => 'Sushi',
			'descripcion' => 'Roll de salmón, kani, pepino, hilos de arroz, aguacate y queso crema envueltos en hoja de arroz.', 
			'valor' => 25000, 
			'tiempo_preparacion' => '45 min', 
			'foto' => 'https://images.rappi.com/products/2090473043-1603465091548.jpg?d=400x400&e=webp', 
		),
        array(
			'nom_plato' => 'Perros Calientes',
			'descripcion' => 'Pan fresco, salchicha tipo americana, chile con carne, queso chedar, ripio de papas, lechuga batavia, guacamole, salsa chedar y sour cream.', 
			'valor' => 18500, 
			'tiempo_preparacion' => '40 mmin', 
			'foto' => 'https://images.rappi.com/products/2091570362-1612642316712.jpg?d=400x400&e=webp', 
		),
        array(
			'nom_plato' => 'Ceviche',
			'descripcion' => 'Ceviche con suero costeño, plátano maduro y aguacate con proteína a elección', 
			'valor' => 30000, 
			'tiempo_preparacion' => '50 mmin', 
			'foto' => 'https://images.rappi.com/products/2092777566-1606228221977.jpg?d=400x400&e=webp', 
		)
    );
	//--
	private $arrayMenu = array(
		array(
			'id_plato' => 1 
		),
		array(
			'id_plato' => 2
		),
		array(
			'id_plato' => 3
		),
		array(
			'id_plato' => 4
		),
		array(
			'id_plato' => 5
		),
		array(
			'id_plato' => 6
		)
	);
	//--
    //-----------------------------------------------------------------------------------------------
    private function seedPlato(){
        foreach($this->arrayPlato as $plato){
            $p=new Plato;
            $p->nom_plato=$plato['nom_plato'];
            $p->descripcion=$plato['descripcion'];
            $p->valor=$plato['valor'];
            $p->tiempo_preparacion=$plato['tiempo_preparacion'];
            $p->foto=$plato['foto'];
            $p->save();
        }
    }
    //-----------------------------------------------------------------------------------------------
	private function seedMenu(){
        foreach($this->arrayMenu as $menu){
            $m=new Menu;
            $m->id_plato=$menu['id_plato'];
			$m->save();
		}
	}
	//-----------------------------------------------------------------------------------------------
    public function run()
    {
		DB::table('menus')->delete();
		DB::table('pedidos')->delete();
        DB::table('platos')->delete();

		$this->call(Roleseeder::class);
		$this->call(UserSeeder::class);

		self::seedPlato();
        $this->command->info('Tabla plato inicializada con datos');

		self::seedMenu();
        $this->command->info('Tabla menu inicializada con datos');

    }
    //-----------------------------------------------------------------------------------------------
    

    

}