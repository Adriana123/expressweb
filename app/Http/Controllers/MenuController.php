<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Menu;
use App\Models\Plato;
use App\Models\Pedido;
use DB;
use Request as req;
use Illuminate\Support\Facades\Auth;

class MenuController extends Controller
{
    //----------------------------------------------------------------------------------------------
    public function getIndex()
    {
        $plato = DB::table('platos')->get();
        return view('menu.index',['arrayPlato'=>$plato]);
    }
    //---------------------------------------------------
    public function getShow($id) 
    {
         /**
         * Detalles del plato.
        */
        $plato=DB::table('platos')->where('id_platos',$id)->get();
        return view('menu.show',array('plato'=>$plato));
    }
    
    //---------------------------------------------------
    public function getCreate() 
    {
        /**
         * Permite agregar un plato.
        */ 
        return view('menu.create'); 
    }
    //---------------------------------------------------
    public function getEdit($id) 
    {
        $plato=Plato::where('id_platos', '=', $id)->firstOrFail();
        if($plato) return view('menu.edit',array('plato'=>$plato));
        else return('no plato');
    }
    //---------------------------------------------------
    public function postCreate(Request $request)
    {
        
        $createPlato = new Plato;
        $createPlato->nom_plato=$request->input('nom_plato');
        $createPlato->tiempo_preparacion=$request->input('tiempo_preparacion');
        $createPlato->valor=$request->input('valor');
        $createPlato->descripcion=$request->input('descripcion');
        
        $image = $request->file('foto');
        $image->move(public_path('/fotos_platos'),$image->getClientOriginalName());

        $createPlato->foto='/fotos_platos/'.$image->getClientOriginalName();
        
        $createPlato->save();
        //return $createPlato;
        return redirect('/menu');
    }
    //---------------------------------------------------
    public function putEdit(Request $request, $id)
    {
        $editPlato = Plato::findOrFail($id);
        $editPlato->nom_plato=$request->input('nom_plato');
        $editPlato->tiempo_preparacion=$request->input('tiempo_preparacion');
        $editPlato->valor=$request->input('valor');
        $editPlato->descripcion=$request->input('descripcion');
        
        
        $image = $request->file('foto');
        $image->move(public_path('/fotos_platos'),$image->getClientOriginalName());

        $editPlato->foto='/fotos_platos/'.$image->getClientOriginalName();
        
        
        $editPlato->save();
        //return $editPlato;
        return redirect('/menu/show/'.$id);
    }
    //---------------------------------------------------
    public function getPedido($id) 
    {
        $plato=DB::table('platos')->where('id_platos',$id)->get();
        return view('menu.pedido',array('plato'=>$plato));
    }
    //---------------------------------------------------
    public function postPedido(Request $request , $id)
    {
        $createPedido = new Pedido;
        $createPedido->id_plato=$id;
        $createPedido->id_usuario=Auth::user()->id;
        $cantidad=$request->input('cantidad');
        $vt=$request->input('valor');

        $createPedido->cantidad=$cantidad;
        $createPedido->valor_total=$vt*$cantidad;
        $createPedido->direccion=$request->input('direccion');
        $createPedido->modo_pago=$request->input('modo_pago');
        
        $createPedido->save();
        return redirect('menu/carrito');
    }
   //---------------------------------------------------
    public function getCarrito() 
    {
        $usuario=Auth::user()->id;
         /**
         * Muestra los productos agregados al carrito de compras.
        */
            if(req::isMethod('get')){
                $carrito = DB::table('platos')
                        ->join('pedidos' , 'platos.id_platos', '=', 'pedidos.id_plato')
                        ->select('platos.nom_plato', 'platos.valor', 'platos.foto', 'pedidos.*')
                        ->where('pedidos.id_usuario',$usuario)
                        ->get();
                return view('menu.carrito',array('carrito'=>$carrito));
    
            }
            else{
            //if(req::isMethod('post')){
                $pedido=Pedido::findOrFail(req::input('pedido'));
                $pedido->delete();
                return redirect('menu/carrito');
            }
    

        
        
    }
    //---------------------------------------------------
    public function deletePedido($id){

        $pedido=Pedido::findOrFail($id);
        $pedido->delete();
        return redirect('/menu/carrito');
    }
    //---------------------------------------------------
    public function deletePlato( $id){

        $plato=Plato::findOrFail($id);
        $plato->delete();
        return redirect('/menu');
    }

}
