<?php
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MenuController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//----------------------------------------------------------------
Route::get('/', [HomeController::class,'getHome']);
//----------------------------------------------------------------
Route::get('menu/pedido/{id}',[MenuController::class, 'getPedido'])->name('au.pedido');
//--------------------------------------------------------------------
Route::any('menu/carrito/',[MenuController::class, 'getCarrito'])->name('au.carrito');
//--------------------------------------------------------------------
//Route::delete('menu/carrito/{id}',[MenuController::class, 'deletePedido']);
//--------------------------------------------------------------------


Route::get('menu', [MenuController::class,'getIndex'])->middleware('auth')->name('au.menu');
//------------------------------------------------------------------
Route::get('menu/show/{id}',[MenuController::class, 'getShow'])->middleware('auth')->name('au.show');
//-------------------------------------------------------------------
Route::delete('menu/show/{id}',[MenuController::class, 'deletePlato']);
//-------------------------------------------------------------------
Route::get('menu/create',[MenuController::class, 'getCreate'])->middleware('auth')->name('admin.create');
//-------------------------------------------------------------
Route::get('menu/edit/{id}', [MenuController::class,'getEdit'])->middleware('auth')->name('admin.edit');
//--------------------------------------------------------------------
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('home');
})->name('dashboard');
//--------------------------------------------------------------------
// Se agrega rutas de tipo POST Y PUT

Route::post('menu/create',[MenuController::class,'postCreate']);
Route::put('menu/edit/{id}',[MenuController::class,'putEdit']);
Route::post('menu/pedido/{id}',[MenuController::class,'postPedido']);
//--------------------------------------------------------------------
